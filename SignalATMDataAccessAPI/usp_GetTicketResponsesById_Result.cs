//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SignalATMDataAccessAPI
{
    using System;
    
    public partial class usp_GetTicketResponsesById_Result
    {
        public int TicketResponsesID { get; set; }
        public System.DateTime EntryDate { get; set; }
        public string UserName { get; set; }
        public string Message { get; set; }
    }
}
