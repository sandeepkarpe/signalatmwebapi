﻿
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Reflection;
using log4net;
using SignalATMDataAccessAPI;

namespace DecintellMembershipAPI.Helper_Code.OAuth2
{
    public class AppOAuthProvider: OAuthAuthorizationServerProvider

    {
        #region Private Properties  
        private readonly Encryption _encry = new Encryption();
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>  
        /// Public client ID property.  
        /// </summary>  
        private readonly string _publicClientId;

        /// <summary>  
        /// Database Store property.  
        /// </summary>  
        private SignalATMEntities databaseManager = new SignalATMEntities();

        #endregion

        #region Default Constructor method.  

        /// <summary>  
        /// Default Constructor method.  
        /// </summary>  
        /// <param name="publicClientId">Public client ID parameter</param>  
        public AppOAuthProvider(string publicClientId)
        {
            //TODO: Pull from configuration  
            if (publicClientId == null)
            {
                throw new ArgumentNullException(nameof(publicClientId));
            }

            // Settings.  
            _publicClientId = publicClientId;
        }

        #endregion

        #region Grant resource owner credentials override method.  

        ///// <summary>  
        ///// Grant resource owner credentials overload method.  
        ///// </summary>  
        ///// <param name="context">Context parameter</param>  
        ///// <returns>Returns when task is completed</returns>  
        //public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        //{
        //    //// Initialization.  
        //    //string usernameVal = context.UserName;
        //    //string passwordVal = context.Password;
        //    //var user = this.databaseManager.usp_Get_Login_Token(usernameVal, passwordVal).ToList();

        //    //// Verification.  
        //    //if (user == null || user.Count() <= 0)
        //    //{
        //    //    // Settings.  
        //    //    context.SetError("invalid_grant", "The user name or password is incorrect.");

        //    //    // Retuen info.  
        //    //    return;
        //    //}

        //    //// Initialization.  
        //    //var claims = new List<Claim>();
        //    //var userInfo = user.FirstOrDefault();

        //    //// Setting  
        //    //claims.Add(new Claim(ClaimTypes.Name, userInfo.USER_ID));

        //    //// Setting Claim Identities for OAUTH 2 protocol.  
        //    //ClaimsIdentity oAuthClaimIdentity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
        //    //ClaimsIdentity cookiesClaimIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationType);

        //    //// Setting user authentication.  
        //    //AuthenticationProperties properties = CreateProperties(userInfo.US);
        //    //AuthenticationTicket ticket = new AuthenticationTicket(oAuthClaimIdentity, properties);

        //    //// Grant access to authorize user.  
        //    //context.Validated(ticket);
        //    //context.Request.Context.Authentication.SignIn(cookiesClaimIdentity);


        //    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
        //    var userName = context.UserName;
        //    var password = context.Password;
        //    Log.Info(_encry.CalculateHash(context.Password));
        //}


        // Chalu code
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                var userName = context.UserName;
                var password = context.Password;

                databaseManager = new SignalATMEntities();
                // Calling the stored procedure for authentication by passing login credential.
                Log.Info(_encry.CalculateHash(context.Password));
                var user = databaseManager.usp_sec_GetAllUserByLoginIDNew(context.UserName, context.Password).FirstOrDefault();
                //var user = 1;
                // usp_Get_ValidUser_Login_Result
                // If user is not null, return the token else return error message.
                if (user != null)
                {
                 
                    var props = new AuthenticationProperties(new Dictionary<string, string>
                    {
                        {
                            "UserName", user.UserName
                        },
                        {
                            "Password", user.Password
                        },
                        {
                            "Role",user.RoleName
                        },
                        
                    });

                    var ticket = new AuthenticationTicket(identity, props);
                    context.Validated(ticket);

                }
                else
                {
                    Log.Info("wrong credential");
                }
            }
            catch (Exception exception)
            {
                // Log the error with stacktrace
                exception.InnerException.ToString();
                Log.Error("\nError:\n" + exception);
                context.SetError("Error", "Service Unavailable");
            }
    }

        #endregion

        #region Token endpoint override method.  

        /// <summary>  
        /// Token endpoint override method  
        /// </summary>  
        /// <param name="context">Context parameter</param>  
        /// <returns>Returns when task is completed</returns>  
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                // Adding.  
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
                
            }

            // Return info.  
            return Task.FromResult<object>(null);
        }

        #endregion

        #region Validate Client authntication override method  

        /// <summary>  
        /// Validate Client authntication override method  
        /// </summary>  
        /// <param name="context">Contect parameter</param>  
        /// <returns>Returns validation of client authentication</returns>  
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.  
            if (context.ClientId == null)
            {
                // Validate Authoorization.  
                context.Validated();
            }

            // Return info.  
            return Task.FromResult<object>(null);
        }

        #endregion

        #region Validate client redirect URI override method  

        /// <summary>  
        /// Validate client redirect URI override method  
        /// </summary>  
        /// <param name="context">Context parmeter</param>  
        /// <returns>Returns validation of client redirect URI</returns>  
        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            // Verification.  
            if (context.ClientId == _publicClientId)
            {
                // Initialization.  
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                // Verification.  
                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    // Validating.  
                    context.Validated();
                }
            }

            // Return info.  
            return Task.FromResult<object>(null);
        }

        #endregion

        #region Create Authentication properties method.  

        /// <summary>  
        /// Create Authentication properties method.  
        /// </summary>  
        /// <param name="USER_ID">User name parameter</param>  
        /// <returns>Returns authenticated properties.</returns>  
        public static AuthenticationProperties CreateProperties(string USER_ID)
        {
            // Settings.  
            

            
            IDictionary<string, string> data = new Dictionary<string, string>
                                               {
                                                   { "USER_ID","Meenakshi"},
                 
                                                   {"Password","1234" }
                  };
            
               

            // Return info.  
            return new AuthenticationProperties(data);
        }


        


    #endregion
    } 
}