﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DecintellMembershipAPI.App_Start
{
    public class Token
    {
        
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            [JsonProperty("token_type")]
            public string TokenType { get; set; }

            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }

            [JsonProperty("refresh_token")]
            public string RefreshToken { get; set; }

        [JsonProperty("OEMId")]
        public string OEMId { get; set; }

        [JsonProperty("RoleName")]
        public string RoleName { get; set; }

        [JsonProperty("User_Id")]
        public string User_Id { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

    }
}