﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace DecintellMembershipAPI.BusinessLogic
{
    public class SMTPEmailBodyBuilder
    {
        public string GetPassqwordResetEmailBody(string strEmailAddress)
        {
            StringBuilder sbPasswordResetEmail = new StringBuilder();
            sbPasswordResetEmail.Append("<!DOCTYPE html PUBLIC ' -//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            sbPasswordResetEmail.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            sbPasswordResetEmail.Append("<head>");
            sbPasswordResetEmail.Append("<meta name='viewport' content='width=device-width' />");
            sbPasswordResetEmail.Append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
            sbPasswordResetEmail.Append("<title> Signal ATM </title>");
            sbPasswordResetEmail.Append("<style>");
            sbPasswordResetEmail.Append("* {");
            sbPasswordResetEmail.Append("margin:0;");
            sbPasswordResetEmail.Append("padding:0;");
            sbPasswordResetEmail.Append("font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;");
            sbPasswordResetEmail.Append("box-sizing:border-box;");
            sbPasswordResetEmail.Append("font-size:13px;}");
            sbPasswordResetEmail.Append("</style></head><body>");
            sbPasswordResetEmail.Append("<table style='background-color:#f6f6f6; width:100%;'>");
            sbPasswordResetEmail.Append("<tr><td></td>");
            sbPasswordResetEmail.Append("<td width='600'><div style='max-width:600px; margin:0 auto;display:block; padding:20px;'>");      
            sbPasswordResetEmail.Append("<table  width='100% ' cellpadding='0' cellspacing='0' style='background:#fff; border:1px solid#e9e9e9; border-radius:3px;'>");
            sbPasswordResetEmail.Append("<tr><td style='background:#1ab394;font-size:16px; color:#fff; font-weight:500;padding:20px;text-align:center; border-radius:3px 3px 0 0;' > Password Reset Successfully !</td>");              
            sbPasswordResetEmail.Append("</tr><tr><td style='padding:20px;'><table width='100%' cellpadding='0' cellspacing='0'>");
            sbPasswordResetEmail.Append("<tr><td style='padding:0 0 20px; vertical-align:top; margin:0; padding:0; line-height:20px; font-size:13px;'>" + strEmailAddress + "<br/>");
            sbPasswordResetEmail.Append("Your password has been successfully changed. <br/>");
            sbPasswordResetEmail.Append("Login with your new password to access your account at Signal ATM, your one stop solution for the best Trading Signals on the Internet. <br/>");
            sbPasswordResetEmail.Append("Make sure to change it at your login </td>");
            sbPasswordResetEmail.Append("</tr><tr><td style='padding:20px 0 20px; line-height:20px;'> Regards, <br/>");
            sbPasswordResetEmail.Append("Team Signal ATM </td></tr></table></td></tr></table>");
            sbPasswordResetEmail.Append("</div></td><td></td></tr></table></body></html>");
            return sbPasswordResetEmail.ToString();
        }
        public string GetRegistrationCompleteEmailBody()
        {
            StringBuilder sbRegistrationCompleteEmail = new StringBuilder();
            sbRegistrationCompleteEmail.Append("<!DOCTYPE html PUBLIC'-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            sbRegistrationCompleteEmail.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            sbRegistrationCompleteEmail.Append("<head>");
            sbRegistrationCompleteEmail.Append("<meta name='viewport' content='width=device-width'/>");
            sbRegistrationCompleteEmail.Append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
            sbRegistrationCompleteEmail.Append("<title> Signal ATM </title>");
            sbRegistrationCompleteEmail.Append("<style>");
            sbRegistrationCompleteEmail.Append("*{");
            sbRegistrationCompleteEmail.Append("margin:0;");
            sbRegistrationCompleteEmail.Append("padding: 0;");
            sbRegistrationCompleteEmail.Append("font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;");
            sbRegistrationCompleteEmail.Append("box-sizing: border-box;");
            sbRegistrationCompleteEmail.Append("font-size:13px;}");
            sbRegistrationCompleteEmail.Append("</style></head><body><table style='background-color:#f6f6f6; width:100%;' >"); 
            sbRegistrationCompleteEmail.Append("<tr><td></td><td width='600'><div style='max-width:600px; margin: 0 auto; display:block; padding:20px;'>");
            sbRegistrationCompleteEmail.Append("<div><table width='100%' cellpadding='0' cellspacing='0' style='background:#fff; border:1px solid#e9e9e9; border-radius:3px;'>");
            sbRegistrationCompleteEmail.Append("<tr><td style='background:#1ab394;font-size:16px; color:#fff; font-weight:500;padding:20px;text-align:center; border-radius:3px 3px 0 0;' > Registration almost Complete</td>");
            sbRegistrationCompleteEmail.Append("</tr><tr><td><table width='100%' cellpadding='0' cellspacing='0'>");
            sbRegistrationCompleteEmail.Append("<tr><td style='padding:20px 0 0px 20px; vertical-align:top;  line-height:20px; font-size:13px;'> We have sent you an account activation email to your registered email address. Please check your inbox for the same.");
            sbRegistrationCompleteEmail.Append("Please note, you won’t be able to proceed without activating your account.</ td ></ tr >");
            sbRegistrationCompleteEmail.Append("<tr><td style='padding:20px 0 0px 20px; vertical-align:top;  line-height:20px; font-size:13px;'><a href='#' style='color:#fff;");
            sbRegistrationCompleteEmail.Append("background-color:#1ab394;  border-color:#1ab394; border-radius:3px; font-size:inherit; padding:10px 12px; text-decoration:none;'> Login</a> </td> ");
            sbRegistrationCompleteEmail.Append("</tr><tr><td style='padding:20px 0 20px 20px; vertical-align: top;  line-height:20px; font-size: 13px;'> Thanks for choosing Signal ATM. </td>");
            sbRegistrationCompleteEmail.Append("</tr></table></td></tr></table></div></td><td></td>");
            sbRegistrationCompleteEmail.Append("</tr></table></body></html>");
            return sbRegistrationCompleteEmail.ToString();
        }

        public string GetActivationEmailBody(string strEmailAddress,string url)
        {
            StringBuilder sbSctivationCompleteEmail = new StringBuilder();
            sbSctivationCompleteEmail.Append("<!DOCTYPE html PUBLIC' -//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            sbSctivationCompleteEmail.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            sbSctivationCompleteEmail.Append("<head>");
            sbSctivationCompleteEmail.Append("<meta name='viewport' content='width=device-width'/>");
            sbSctivationCompleteEmail.Append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
            sbSctivationCompleteEmail.Append("<title> Signal ATM </title>");
            sbSctivationCompleteEmail.Append("<style>");
            sbSctivationCompleteEmail.Append("*{");
            sbSctivationCompleteEmail.Append("margin:0;");
            sbSctivationCompleteEmail.Append("padding:0;");
            sbSctivationCompleteEmail.Append("font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;");
            sbSctivationCompleteEmail.Append("box-sizing:border-box;");
            sbSctivationCompleteEmail.Append("font-size:13px;");
            sbSctivationCompleteEmail.Append("}");
            sbSctivationCompleteEmail.Append("</style>");
            sbSctivationCompleteEmail.Append("</head>");
            sbSctivationCompleteEmail.Append("<body>");
            sbSctivationCompleteEmail.Append("<table style='background-color:#f6f6f6; width:100%;'>"); 
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td></td>");
            sbSctivationCompleteEmail.Append("<td width='600'><div style='max-width:600px; margin:0 auto;display:block; padding:20px;'>");
            sbSctivationCompleteEmail.Append("<div>");
            sbSctivationCompleteEmail.Append("<table width='100%' cellpadding='0' cellspacing='0' style='background:#fff; border:1px solid#e9e9e9; border-radius:3px;'>");             
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td style='background:#1ab394;font-size:16px; color:#fff; font-weight:500;padding:20px;text-align: center; border-radius: 3px 3px 0 0;'> Registration almost Complete</td>");
            sbSctivationCompleteEmail.Append("</tr>");
            //sbSctivationCompleteEmail.Append("<tr>");
            //sbSctivationCompleteEmail.Append("<td>< table width='100%' cellpadding='0' cellspacing='0'>");
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td style='padding:20px 0 0px 20px; vertical-align:top; line-height:20px; font-size:13px;'> Hello " + strEmailAddress +"<br/>");
            sbSctivationCompleteEmail.Append("Your application has been successfully received.");
            sbSctivationCompleteEmail.Append("<br/>");
            sbSctivationCompleteEmail.Append("Please ensure that you have read and acknowledged Signal ATM’s Terms & Conditions including Privacy Policy.</td>");
            sbSctivationCompleteEmail.Append("</tr>");
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td style='padding:20px 0 0px 20px; vertical-align:top; line-height:20px; font-size:13px;'>");
            sbSctivationCompleteEmail.Append("To activate,<a href='" + url + "'style='color:#fff;background-color:#1ab394';>click here </a>");
            sbSctivationCompleteEmail.Append("<br/>");
            sbSctivationCompleteEmail.Append("Or click on the link bellow ");
            sbSctivationCompleteEmail.Append("<br/>");
            sbSctivationCompleteEmail.Append("<br/>");
            sbSctivationCompleteEmail.Append(""+ url +"</td>");
            sbSctivationCompleteEmail.Append("</tr>");
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td style='padding:20px 0 0px 20px; vertical-align:top; line-height:20px; font-size:13px;'><!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
            sbSctivationCompleteEmail.Append("<HTML>");
            sbSctivationCompleteEmail.Append("<!--@page {margin: 0.79in}");
            sbSctivationCompleteEmail.Append("P {margin-bottom: 0.08in}");
            sbSctivationCompleteEmail.Append("-->");
            sbSctivationCompleteEmail.Append("<BODY DIR='LTR'>");
            sbSctivationCompleteEmail.Append("This link will expire in 30 calendar days from the time  this email sent. Please activate your account within this period.");
            sbSctivationCompleteEmail.Append("<br/><br/>");
            sbSctivationCompleteEmail.Append("Your journey on subscribing trading related signals for  your preferred markets across the globe is about to begin. </td>");
            sbSctivationCompleteEmail.Append("</tr><tr><td style='padding:20px 0 20px 20px; line-height:20px;'> Regards,<br/>Team Signal ATM </td>");
            sbSctivationCompleteEmail.Append("</tr></table></td></tr></table></div></td><td></td></tr></table></body></html>");
            return sbSctivationCompleteEmail.ToString();

        }
        public string GetActivationPasswordBody(string strEmailAddress, string url)
        {
            StringBuilder sbSctivationCompleteEmail = new StringBuilder();
            sbSctivationCompleteEmail.Append("<!DOCTYPE html PUBLIC' -//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            sbSctivationCompleteEmail.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            sbSctivationCompleteEmail.Append("<head>");
            sbSctivationCompleteEmail.Append("<meta name='viewport' content='width=device-width'/>");
            sbSctivationCompleteEmail.Append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
            sbSctivationCompleteEmail.Append("<title> Signal ATM </title>");
            sbSctivationCompleteEmail.Append("<style>");
            sbSctivationCompleteEmail.Append("*{");
            sbSctivationCompleteEmail.Append("margin:0;");
            sbSctivationCompleteEmail.Append("padding:0;");
            sbSctivationCompleteEmail.Append("font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;");
            sbSctivationCompleteEmail.Append("box-sizing:border-box;");
            sbSctivationCompleteEmail.Append("font-size:13px;");
            sbSctivationCompleteEmail.Append("}");
            sbSctivationCompleteEmail.Append("</style>");
            sbSctivationCompleteEmail.Append("</head>");
            sbSctivationCompleteEmail.Append("<body>");
            sbSctivationCompleteEmail.Append("<table style='background-color:#f6f6f6; width:100%;'>");
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td></td>");
            sbSctivationCompleteEmail.Append("<td width='600'><div style='max-width:600px; margin:0 auto;display:block; padding:20px;'>");
            sbSctivationCompleteEmail.Append("<div>");
            sbSctivationCompleteEmail.Append("<table width='100%' cellpadding='0' cellspacing='0' style='background:#fff; border:1px solid#e9e9e9; border-radius:3px;'>");
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td style='background:#1ab394;font-size:16px; color:#fff; font-weight:500;padding:20px;text-align: center; border-radius: 3px 3px 0 0;'> Forgot Password</td>");
            sbSctivationCompleteEmail.Append("</tr>");           
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td style='padding:20px 0 0px 20px; vertical-align:top; line-height:20px; font-size:13px;'> Hello " + strEmailAddress + "<br/>");
            sbSctivationCompleteEmail.Append("Your application has been successfully received.");
            sbSctivationCompleteEmail.Append("Please ensure that you have read and acknowledged Signal ATM’s Terms &Conditions including Privacy Policy.</td>");
            sbSctivationCompleteEmail.Append("</tr>");
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td style='padding:20px 0 0px 20px; vertical-align:top; line-height:20px; font-size:13px;'>");
            sbSctivationCompleteEmail.Append("To activate,<a href='" + url + "'style='color:#fff;background-color:#1ab394';>click here </a>");
            sbSctivationCompleteEmail.Append("<br/>");
            sbSctivationCompleteEmail.Append("Or click on the link bellow ");
            sbSctivationCompleteEmail.Append("<br/>");
            sbSctivationCompleteEmail.Append("<br/>");
            sbSctivationCompleteEmail.Append("" + url + "</td>");
            sbSctivationCompleteEmail.Append("</tr>");
            sbSctivationCompleteEmail.Append("<tr>");
            sbSctivationCompleteEmail.Append("<td style='padding:20px 0 0px 20px; vertical-align:top; line-height:20px; font-size:13px;'><!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
            sbSctivationCompleteEmail.Append("<HTML>");
            sbSctivationCompleteEmail.Append("<!--@page {margin: 0.79in}");
            sbSctivationCompleteEmail.Append("P {margin-bottom: 0.08in}");
            sbSctivationCompleteEmail.Append("-->");
            sbSctivationCompleteEmail.Append("<BODY DIR='LTR'>");
            sbSctivationCompleteEmail.Append("This link will expire in 30 calendar days from the time  this email sent. Please reset your password within this period.");           
            sbSctivationCompleteEmail.Append("</tr><tr><td style='padding:20px 0 20px 20px; line-height:20px;'> Regards,<br/>Team Signal ATM </td>");
            sbSctivationCompleteEmail.Append("</tr></table></td></tr></table></div></td><td></td></tr></table></body></html>");
            return sbSctivationCompleteEmail.ToString();

        }
    }
}