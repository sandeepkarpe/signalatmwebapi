﻿using DecintellMembershipAPI.Models.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace DecintellMembershipAPI.Controllers
{
    [RoutePrefix("api")]
    public class SignalManagementController : BaseController
    {

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly SignalManagementRepository signalManagementRepository = new SignalManagementRepository();

        [HttpGet]
        [Route("signalmanagement/savenewsignal")]
        public IHttpActionResult SaveNewSignal(int InstrumentId, int StlD, int SigProID, int BuySellID, decimal StrikePrice, decimal TargetPrice,
                                decimal StopLoss, decimal InstrumentPrice, string DirectionCheck, string strComments, int MINTimeFrameID, int MaxTimeFrameID,
                                DateTime MaxTimeFrame, int MinValidityID, int MaxValidityID, DateTime MaxValidityDate, int ReliabilityID, int RiskID,
                                int ProcessStatusID, int SuccessStatusID, decimal ClosingPips,decimal ClosingPercent, DateTime EntryDate, DateTime ClosingDate, string UpdatedBy, decimal LivePrice)
        {
            var responseJson = signalManagementRepository.SaveNewSignal(InstrumentId, StlD, SigProID, BuySellID, StrikePrice, TargetPrice,
                            StopLoss, InstrumentPrice, DirectionCheck, strComments, MINTimeFrameID, MaxTimeFrameID,
                            MaxTimeFrame, MinValidityID, MaxValidityID, MaxValidityDate, ReliabilityID, RiskID,
                            ProcessStatusID, SuccessStatusID, ClosingPips,ClosingPercent, EntryDate, ClosingDate, UpdatedBy, LivePrice);
            return Response(responseJson);
        }

        [HttpGet]
        [Route("signalmanagement/GetInstrumentdetailsbyinstrumentID")]
        public IHttpActionResult GetInstrumentDetailsByInstrumentID(int InstrumentID)
        {
            var responseJson = signalManagementRepository.GetInstrumentDetailsByInstrumentID(InstrumentID);
            return Response(responseJson);
        }

    }
}