﻿using DecintellMembershipAPI.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using log4net;
using System.IO;
using DecintellMembershipAPI.Models.IRepository;
using DecintellMembershipAPI.BusinessLogic;


namespace DecintellMembershipAPI.Controllers
{
   // [Authorize]
    [RoutePrefix("api")]
    public class MembershipProviderController : BaseController
    {
        #region Private Variables

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly DecintellMembershipRepository _decintellMembershipRepository = new DecintellMembershipRepository();

        #endregion

        #region GetClientMembership

        /// <summary>
        /// Fetching valid user
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <param name="UserType"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("providers/ClientMembership")]
        public IHttpActionResult GetClientMembership(string UserID, string Password)
        {
          
            var responseJson = _decintellMembershipRepository.GetValidUser(UserID, Password);
            return Response(responseJson);
        }
        #endregion    
        
    }
}
