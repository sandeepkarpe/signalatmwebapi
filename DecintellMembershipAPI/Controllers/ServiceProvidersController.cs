﻿using DecintellMembershipAPI.Models.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;




namespace DecintellMembershipAPI.Controllers
{
    [RoutePrefix("api")]
    public class ServiceProvidersController : BaseController
    {

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ServiceProvidersRepository serviceProvidersRepository = new ServiceProvidersRepository();
        private readonly EmailRepository emailRepository = new EmailRepository();

        [Route("serviceproviders/getcountries")]
        public IHttpActionResult GetCountries()
        {
            var responseJson = serviceProvidersRepository.GetCountries();
            return Response(responseJson);            
        }

        [Route("serviceproviders/getmarkets")]
        public IHttpActionResult GetMarkets()
        {
            Log.Debug("Call");
            var responseJson = serviceProvidersRepository.GetMarkets();
            return Response(responseJson);
        }

        [Route("serviceproviders/getinstrumentsbymarket")]
        public IHttpActionResult GetInstrumentsByMarket(int MarketId)
        {
            var responseJson = serviceProvidersRepository.GetAllInstrumentsbyMarketId(MarketId);
            return Response(responseJson);
        }

        [Route("serviceproviders/getallsignaltypes")]
        public IHttpActionResult GetAllSignalTypes()
        {
            var responseJson = serviceProvidersRepository.GetAllSignalType();
            return Response(responseJson);
        }

        [Route("serviceproviders/getallrisks")]
        public IHttpActionResult GetAllRisks()
        {
            var responseJson = serviceProvidersRepository.GetAllRisks();
            return Response(responseJson);
        }

        [Route("serviceproviders/getallperiods")]
        public IHttpActionResult GetAllPeriods()
        {
            var responseJson = serviceProvidersRepository.GetAllPeriods();
            return Response(responseJson);
        }

        [Route("serviceproviders/getalltimeframes")]
        public IHttpActionResult GetAllTimeFrames()
        {
            var  responseJson = serviceProvidersRepository.GetAllTimeFrames();
            return Response(responseJson);
        }

        [Route("serviceproviders/getallreliability")]
        public IHttpActionResult GetAllReliability()
        {
            var responseJson = serviceProvidersRepository.GetAllReliability();
            return Response(responseJson);
        }

        [Route("serviceproviders/getbuyselllist")]
        public IHttpActionResult GetBuySellList()
        {
            var responseJson = serviceProvidersRepository.GetBuySellList();
            return Response(responseJson);
        }       

        [Route("serviceproviders/sendpasswordresetemail")]
        public void SendPasswordResetEmail(string mailTo, string mailSubject, string mailBody)
        {
            emailRepository.SendPasswordResetEmail(mailTo, mailSubject, mailBody);            
        }

        //[HttpPost]
        [Route("serviceproviders/sendregistrationemail")]
        public void SendRegistartionEmail(string mailTo, string mailSubject)
        {            
            emailRepository.SendRegistartionEmail(mailTo, mailSubject);
        }

        //[HttpPost]
        [Route("serviceproviders/sendregistrationemailtest")]
        public void SendRegistartionEmailtest()
        {
            Log.Info("SendRegistartionEmailtest called");
            string mailTo = "ajaydabhade@gmail.com";
            string mailSubject = "Test email";
            emailRepository.SendRegistartionEmail(mailTo, mailSubject);
        }

        [HttpGet]
        [Route("serviceproviders/sendactivationemail")]
        public void SendActivationEmail(string mailTo, string mailSubject,string url)
        {
            emailRepository.SendActivationEmail(mailTo, mailSubject,url);
        }

        [HttpGet]
        [Route("serviceproviders/sendpasswordresetemaillink")]
        public void SendPasswordResetEmaillink(string mailTo, string mailSubject, string url)
        {
            string mailBody = "";
            emailRepository.SendPasswordResetEmaillink(mailTo, mailSubject, mailBody, url);
        }

        [HttpPost]
        [Route("serviceproviders/savebasicconfiguration")]
        public void savebasicconfiguration(int countryID, int timeZoneID, int userID, int marketId, string instrumentAr)
        {
            serviceProvidersRepository.SaveBasicConfig(countryID, timeZoneID, userID, marketId, instrumentAr);
        }
    }
}