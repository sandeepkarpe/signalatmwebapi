﻿using DecintellMembershipAPI.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using log4net;
using System.IO;
using DecintellMembershipAPI.Models.IRepository;
using DecintellMembershipAPI.BusinessLogic;
using System.Data;
using System.Collections;



namespace DecintellMembershipAPI.Controllers
{
    /// <summary></summary>
    /// <seealso cref="DecintellMembershipAPI.Controllers.BaseController" />
    //SignalATMDataAccessAPI sqlAccess = new SignalATMDataAccessAPI();
    [RoutePrefix("api")]
    public class MobileAppProvidersController : BaseController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly MobileAppProvidersRepository mobileAppProvidersRepository = new MobileAppProvidersRepository();
        private readonly EmailRepository emailRepository = new EmailRepository();

        /// <summary>Gets the time zones.</summary>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/gettimezones")]
        public IHttpActionResult GetTimeZones()
        {
            
            var responseJson = mobileAppProvidersRepository.GetTimeZones();
            return Response(responseJson);
        }


        /// <summary>Gets the time zones by country identifier.</summary>
        /// <param name="CountryId">The country identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/gettimezonesbycountryid")]
        public IHttpActionResult GetTimeZonesByCountryId(int CountryId)
        {
            var responseJson = mobileAppProvidersRepository.GetTimeZonesByCountryId(CountryId);
            return Response(responseJson);
        }

        /// <summary>Gets the user plan.</summary>
        /// <param name="RoleId">The role identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getuserplan")]
        public IHttpActionResult GetUserPlan(int RoleId)
        {
            var responseJson = mobileAppProvidersRepository.GetUserPlan(RoleId);
            return Response(responseJson);
        }

        /// <summary>Gets the subscription plan.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getsubscriptionplan")]
        public IHttpActionResult GetSubscriptionPlan(int UserID)
        {
            var responseJson = mobileAppProvidersRepository.GetSubscriptionPlan(UserID);
            return Response(responseJson);
        }

        /// <summary>Gets the personal details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getpersonaldetails")]
        public IHttpActionResult GetPersonalDetails(int UserID)
        {
            var responseJson = mobileAppProvidersRepository.GetPersonalDetails(UserID);
            return Response(responseJson);
        }

        /// <summary>Updates the personal details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Email">The email.</param>
        /// <param name="UserPhoneNO">The user phone no.</param>
        /// <param name="Gender">The gender.</param>
        /// <param name="UserUrl">The user URL.</param>
        /// <param name="UserAddress">The user address.</param>
        /// <param name="CountryID">The country identifier.</param>
        /// <param name="State">The state.</param>
        /// <param name="City">The city.</param>
        /// <param name="TimeZone">The time zone.</param>
        /// <param name="AboutMe">The about me.</param>
        /// <param name="UserReferenceID">The user reference identifier.</param>
        /// <param name="UserImage">The user image.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/updatepersonaldetails")]
        public IHttpActionResult UpdatePersonalDetails(int UserID, string UserName, string Email, string UserPhoneNO, int Gender, string UserUrl, string UserAddress, int CountryID, string State, string City, float TimeZone, string AboutMe, int UserReferenceID, byte[] UserImage)
        {
            var responseJson = mobileAppProvidersRepository.UpdatePersonalDetails(UserID, UserName, Email, UserPhoneNO, Gender, UserUrl, UserAddress, CountryID, State, City, TimeZone, AboutMe, UserReferenceID, UserImage);
            return Response(responseJson);
        }

        /// <summary>Saves the personal details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Email">The email.</param>
        /// <param name="UserPhoneNO">The user phone no.</param>
        /// <param name="Gender">The gender.</param>
        /// <param name="UserUrl">The user URL.</param>
        /// <param name="UserAddress">The user address.</param>
        /// <param name="CountryID">The country identifier.</param>
        /// <param name="State">The state.</param>
        /// <param name="City">The city.</param>
        /// <param name="TimeZone">The time zone.</param>
        /// <param name="AboutMe">The about me.</param>
        /// <param name="UserReferenceID">The user reference identifier.</param>
        /// <param name="UserImage">The user image.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/savepersonaldetails")]
        public IHttpActionResult SavePersonalDetails(int UserID, string UserName, string Email, string UserPhoneNO, int Gender, string UserUrl, string UserAddress, int CountryID, string State, string City, float TimeZone, string AboutMe, int UserReferenceID, byte[] UserImage)
        {
            var responseJson = mobileAppProvidersRepository.SavePersonalDetails(UserID, UserName, Email, UserPhoneNO, Gender, UserUrl, UserAddress, CountryID, State, City, TimeZone, AboutMe, UserReferenceID, UserImage);
            return Response(responseJson);
        }

        /// <summary>Gets the trader compare performance.</summary>
        /// <param name="Period1">The period1.</param>
        /// <param name="Period2">The period2.</param>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/gettradercompareperformance")]
        public IHttpActionResult GetTraderComparePerformance(int Period1, int Period2, int UserID)
        {
            var responseJson = mobileAppProvidersRepository.GetTraderComparePerformance(Period1, Period2, UserID);
            return Response(responseJson);
        }

        /// <summary>Gets the signal provider perfromance details.</summary>
        /// <param name="SigProID">The sig pro identifier.</param>
        /// <param name="MarketID">The market identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getsignalproviderperformancedetails")]
        public IHttpActionResult GetSignalProviderPerfromanceDetails(int SigProID, int MarketID)
        {
            var responseJson = mobileAppProvidersRepository.GetSignalProviderPerfromanceDetails(SigProID, MarketID);
            return Response(responseJson);
        }

        /// <summary>Uploads the user documents.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="UserRole">The user role.</param>
        /// <param name="imgUserPhoto">The user photo.</param>
        /// <param name="UserProfile">The user profile.</param>
        /// <param name="UserIdentity">The user identity.</param>
        /// <param name="UserAddressProof">The user address proof.</param>
        /// <param name="UserBankDetails">The user bank details.</param>
        /// <param name="IMGUserChequeStatement">The user cheque statement.</param>
        /// <param name="IsApproved">if set to <c>true</c> [is approved].</param>
        /// <param name="FlagUserPhoto">The flag user photo.</param>
        /// <param name="FlagUserProfile">The flag user profile.</param>
        /// <param name="FlagUserIdentity">The flag user identity.</param>
        /// <param name="FlagUserAddressProof">The flag user address proof.</param>
        /// <param name="FlagUserChequeStatement">The flag user cheque statement.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/uploaduserdocuments")]
        public IHttpActionResult UploadUserDocuments(int UserID, int UserRole, byte[] imgUserPhoto, byte[] UserProfile, byte[] UserIdentity, byte[] UserAddressProof, string UserBankDetails, byte[] imgUserChequeStatement, bool IsApproved, int FlagUserPhoto, int FlagUserProfile, int FlagUserIdentity, int FlagUserAddressProof, int FlagUserChequeStatement)
        {
            var responseJson = mobileAppProvidersRepository.UploadUserDocuments(UserID, UserRole, imgUserPhoto, UserProfile, UserIdentity, UserAddressProof, UserBankDetails, imgUserChequeStatement, IsApproved, FlagUserPhoto, FlagUserProfile, FlagUserIdentity, FlagUserAddressProof, FlagUserChequeStatement);
            return Response(responseJson);
        }

        /// <summary>Gets the user document details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getuserdocumentdetails")]
        public IHttpActionResult GetUserDocumentDetails(int UserID)
        {
            var responseJson = mobileAppProvidersRepository.GetUserDocumentDetails(UserID);
            return Response(responseJson);
        }

        /// <summary>Gets the signal library for trader.</summary>
        /// <param name="TraderID">The trader identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getsignallibraryfortrader")]
        public IHttpActionResult GetSignalLibraryForTrader(int TraderID)
        {
            var responseJson = mobileAppProvidersRepository.GetSignalLibraryForTrader(TraderID);
            return Response(responseJson);
        }

        /// <summary>Gets the pinned signals.</summary>
        /// <param name="TraderID">The trader identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getpinnedsignals")]
        public IHttpActionResult GetPinnedSignals(int TraderID)
        {
            var responseJson = mobileAppProvidersRepository.GetPinnedSignals(TraderID);
            return Response(responseJson);
        }

        /// <summary>Gets the signal closed.</summary>
        /// <param name="SigProID">The sig pro identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getsignalclosed")]
        public IHttpActionResult GetSignalClosed(int SigProID)
        {
            var responseJson = mobileAppProvidersRepository.GetSignalClosed(SigProID);
            return Response(responseJson);
        }

        /// <summary>Gets the ticket responses by identifier.</summary>
        /// <param name="TraderID">The trader identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getticketresponsebyid")]
        public IHttpActionResult GetTicketResponsesById(int TraderID)
        {
            var responseJson = mobileAppProvidersRepository.GetTicketResponsesById(TraderID);
            return Response(responseJson);
        }

        /// <summary>Gets the responses MSG by identifier.</summary>
        /// <param name="TicketResponsesID">The ticket responses identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getresponsesmsgbyid")]
        public IHttpActionResult GetResponsesMsgById(int TicketResponsesID)
        {
            var responseJson = mobileAppProvidersRepository.GetResponsesMsgById(TicketResponsesID);
            return Response(responseJson);
        }


        /// <summary>Gets the subscribed plan details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getsubscribedplandetails")]
        public IHttpActionResult GetSubscribedPlanDetails(int UserID)
        {
            var responseJson = mobileAppProvidersRepository.GetSubscribedPlanDetails(UserID);
            return Response(responseJson);
        }

        /// <summary>Gets the promo coupons.</summary>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getpromocoupons")]
        public IHttpActionResult GetPromoCoupons()
        {
            var responseJson = mobileAppProvidersRepository.GetPromoCoupons();
            return Response(responseJson);
        }

        /// <summary>Getmsts the feedback.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getmstfeedback")]
        public IHttpActionResult GetmstFeedback(int UserID)
        {
            var responseJson = mobileAppProvidersRepository.GetmstFeedback(UserID);
            return Response(responseJson);
        }


        /// <summary>Saves the feedback.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="FeedbackSubject">The feedback subject.</param>
        /// <param name="FeedbackComment">The feedback comment.</param>
        /// <param name="FeedbackFile">The feedback file.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/savefeedback")]
        public IHttpActionResult SaveFeedback(int UserID, string FeedbackSubject, string FeedbackComment, byte[] FeedbackFile)
        {
            var responseJson = mobileAppProvidersRepository.SaveFeedback(UserID, FeedbackSubject, FeedbackComment, FeedbackFile);
            return Response(responseJson);
        }

        /// <summary>Saves the ticket details.</summary>
        /// <param name="FromID">From identifier.</param>
        /// <param name="ToID">To identifier.</param>
        /// <param name="Subject">The subject.</param>
        /// <param name="Message">The message.</param>
        /// <param name="TicketStatusID">The ticket status identifier.</param>
        /// <param name="imgChooseScreen">The img choose screen.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/saveticketdetails")]
        public IHttpActionResult SaveTicketDetails(int FromID, int ToID, string Subject, string Message, int TicketStatusID, byte[] imgChooseScreen)
        {
            var responseJson = mobileAppProvidersRepository.SaveTicketDetails(FromID, ToID, Subject, Message, TicketStatusID, imgChooseScreen);
            return Response(responseJson);
        }

        /// <summary>Saves the ticket responses.</summary>
        /// <param name="TicketID">The ticket identifier.</param>
        /// <param name="Message">The message.</param>
        /// <param name="TicketStatusID">The ticket status identifier.</param>
        /// <param name="ResponsebyID">The responseby identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/saveticketresponses")]
        public IHttpActionResult SaveTicketResponses(int TicketID, string Message, int TicketStatusID, int ResponsebyID)
        {
            var responseJson = mobileAppProvidersRepository.SaveTicketResponses(TicketID, Message, TicketStatusID, ResponsebyID);
            return Response(responseJson);
        }

        /// <summary>Gets the ticket status list.</summary>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getticketstatuslist")]
        public IHttpActionResult GetTicketStatusList()
        {
            var responseJson = mobileAppProvidersRepository.GetTicketStatusList();
            return Response(responseJson);
        }


        /// <summary>Gets the signal library.</summary>
        /// <param name="sigProID">The sig pro identifier.</param>
        /// <returns></returns>
        //[HttpPost]
        //[Route("mobileappproviders/getsignallibrary")]
        //public IHttpActionResult GetSignalLibrary(int sigProID)
        //{
        //    var responseJson = mobileAppProvidersRepository.GetSignalLibrary(sigProID);
        //    return Response(responseJson);
        //}

        //[HttpPost]
        //[Route("mobileappproviders/getsignallibrary")]
        //public DataTable GetSignalLibrary(int sigProID)
        //{

        //    //SQLDataAccess sqlDataAccess = new SQLDataAccess();
        //    DataTable dt = new DataTable();
        //    Hashtable para = new Hashtable();
        //    para.Add("@SigProID", sigProID);          
        //    dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SP_SignalLibrary", para);
        //    return dt;
        //}


        /// <summary>Gets the signaldashboard.</summary>
        /// <param name="sigProID">The sig pro identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getsignaldashboard")]
        public IHttpActionResult GetSignaldashboard(int sigProID)
        {
            var responseJson = mobileAppProvidersRepository.GetSignaldashboard(sigProID);
            return Response(responseJson);
        }


        /// <summary>Gets the user earning cashout request.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="RoleID">The role identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getuserearningcashoutrequest")]
        public IHttpActionResult GetUserEarningCashoutRequest(int UserID, int RoleID)
        {
            var responseJson = mobileAppProvidersRepository.GetUserEarningCashoutRequest(UserID, RoleID);
            return Response(responseJson);
        }

        /// <summary>Gets the user wallet balance.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="RoleID">The role identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getuserwallatbalance")]
        public IHttpActionResult GetUserWalletBalance(int UserID, int RoleID)
        {
            var responseJson = mobileAppProvidersRepository.GetUserWalletBalance(UserID, RoleID);
            return Response(responseJson);
        }

        /// <summary>Saves the user cashout earning request details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="RoleID">The role identifier.</param>
        /// <param name="RequestAmout">The request amout.</param>
        /// <param name="CurrentWallentAmount">The current wallent amount.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/saveusercashoutearningrequestdetails")]
        public IHttpActionResult SaveUserCashoutEarningRequestDetails(int UserID, int RoleID, decimal RequestAmout, decimal CurrentWallentAmount)
        {
            var responseJson = mobileAppProvidersRepository.SaveUserCashoutEarningRequestDetails(UserID, RoleID, RequestAmout, CurrentWallentAmount);
            return Response(responseJson);
        }


        /// <summary>Referrers the performance deatils.</summary>
        /// <param name="RefferedID">The reffered identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getrefferperformancedetails")]
        public IHttpActionResult ReferrerPerformanceDeatils(int RefferedID)
        {
            var responseJson = mobileAppProvidersRepository.ReferrerPerformanceDeatils(RefferedID);
            return Response(responseJson);
        }

        /// <summary>Gets the empanelment details.</summary>
        /// <param name="SignalProID">The signal pro identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("mobileappproviders/getempanelmentdetails")]
        public IHttpActionResult GetEmpanelmentDetails(int SignalProID)
        {
            var responseJson = mobileAppProvidersRepository.GetEmpanelmentDetails(SignalProID);
            return Response(responseJson);
        }
    }
}