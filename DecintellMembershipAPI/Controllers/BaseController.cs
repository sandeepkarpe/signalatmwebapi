﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace DecintellMembershipAPI.Controllers
{
    public class BaseController : ApiController
    {
        //private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IHttpActionResult Response(JObject responseJson)
        {
            try
            {
                var statusCode = Convert.ToInt32(responseJson.SelectToken("statusCode").Value<string>());
                //responseJson.Remove("statusCode");
                switch (statusCode)
                {
                    case 200:
                        return Content(HttpStatusCode.OK, responseJson);
                    case 201:
                        return Content(HttpStatusCode.Created, responseJson);
                    case 404:
                        return Content(HttpStatusCode.NotFound, responseJson);
                    case 400:
                        return Content(HttpStatusCode.BadRequest, responseJson);
                    case 401:
                        return Content(HttpStatusCode.Unauthorized, responseJson);
                    case 204:
                        return Content(HttpStatusCode.NoContent, responseJson);
                    case 304:
                        return new HttpActionResult(HttpStatusCode.NotModified, responseJson.ToString());
                    default:
                        return Content(HttpStatusCode.ServiceUnavailable, responseJson);
                }
            }
            catch (Exception exception)
            {
                //Log.Error(exception);
                throw;
            }
        }

        public IHttpActionResult ModelStateErrorResponse()
        {
            var errors = new List<string>();
            foreach (var state in ModelState)
            {
                foreach (var error in state.Value.Errors)
                {
                    errors.Add(error.ErrorMessage);
                }
            }

            var responseValue = new ModelStateError
            {
                StatusCode = 400,
                Error = errors,
                Data = null,
                Status = false
            };
            return Content(HttpStatusCode.BadRequest, responseValue);
        }

        public class ModelStateError
        {
            [JsonProperty("statusCode")]
            public int StatusCode { get; set; }

            [JsonProperty("error")]
            public List<string> Error { get; set; }

            [JsonProperty("data")]
            public string Data { get; set; }

            [JsonProperty("status")]
            public bool Status { get; set; }

        }
    }
    public class HttpActionResult : IHttpActionResult
    {
        private readonly string _message;
        private readonly HttpStatusCode _statusCode;
 
        public HttpActionResult(HttpStatusCode statusCode, string message)
        {
            _statusCode = statusCode;
            _message = message;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var httpContent = new StringContent(_message);
            httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = new HttpResponseMessage(_statusCode)
            {
                Content = httpContent
            };
            return Task.FromResult(response);
        }


    }
}
