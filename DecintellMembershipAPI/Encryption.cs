﻿using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using Microsoft.VisualBasic;

namespace DecintellMembershipAPI
{
    public class Encryption
    {

        /// <summary>
        /// Calculates the hash.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public string CalculateHash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            // Compute hash from the bytes of text.
            md5.ComputeHash(Encoding.ASCII.GetBytes(text));

            // Get hash result after compute it.
            var hashResult = md5.Hash;

            var strBuilder = new StringBuilder();
            foreach (var t in hashResult)
                strBuilder.Append(t.ToString("x2"));

            return strBuilder.ToString();
        }

        /// <summary>
        /// Encrypts the specified input string.
        /// </summary>
        /// <param name="InputString">The input string.</param>
        /// <returns></returns>
        public static string Encrypt(string InputString)
        {
            if (Convert.IsDBNull(InputString))
            {
                return "";
            }
            if (InputString == string.Empty)
            {
                return "";
            }
            try
            {
                //Dim mCryptProv As SymmetricAlgorithm
                MemoryStream mMemStr = default(MemoryStream);

                //Encrypt the Data in  the textbox txtData, show that in MessageBox 
                //and write back to textbox   
                //Here You can provide the name of any class which supportsSymmetricAlgorithm Like DES(mCryptProv = SymmetricAlgorithm.Create("Rijndael"))
                TripleDESCryptoServiceProvider mCryptProv = new TripleDESCryptoServiceProvider();

                //The encrypted data will be stored in the memory, so we need a memory stream 
                //object
                mMemStr = new MemoryStream();
                byte[] iv = null;
                byte[] key = null;
                key = System.Text.Encoding.ASCII.GetBytes("E$@Efd45$#Wddsf#0WSdPSD4");
                iv = System.Text.Encoding.ASCII.GetBytes("5%&ERfd4");
                //Create the ICryptTransform Object. 
                ICryptoTransform mTransform = mCryptProv.CreateEncryptor(key, iv);

                //Create the Crypto Stream for writing, and pass that MemoryStream 
                //(where to write after encryption), and ICryptoTransform Object.
                CryptoStream mCSWriter = new CryptoStream(mMemStr, mTransform, CryptoStreamMode.Write);
                //This StreamWriter object will be used to write 
                //Encrypted data to the Memory Stream 

                //Pass it the object of CryptoStream 
                StreamWriter mSWriter = new StreamWriter(mCSWriter);
                mSWriter.Write(InputString);

                //Write data after encryption 
                mSWriter.Flush();
                //Make sure to write everything from Stream 
                mCSWriter.FlushFinalBlock();
                //Flush the CryptoStream as well 

                //The Data has been written in the Memory, But we need to display that 
                //back to text box and want to show in Messagebox. So do the following

                //Create byte array to receive data
                byte[] mBytes = new byte[mMemStr.Length];
                mMemStr.Position = 0;
                //Move to beginning of data
                //Read All data from the memory
                mMemStr.Read(mBytes, 0, (int)mMemStr.Length);

                //But this data is in Bytes, and we need to convert it to string.
                //String conversion address to the problem of Encoding format. We are
                //using UTF8 Encoding to encode data from byte to string

                string mEncData = ConvertByteArrayToString(mBytes);
                byte[] inputbytes = ConvertStringToByteArray(mEncData);

                mMemStr.Position = 0;
                mTransform = mCryptProv.CreateDecryptor(key, iv);
                CryptoStream mCSReader = new CryptoStream(mMemStr, mTransform, CryptoStreamMode.Read);
                StreamReader mStrREader = new StreamReader(mCSReader);
                string mDecData = mStrREader.ReadToEnd();

                return (mEncData);

            }
            catch (Exception ex)
            {
                return ex.ToString();

            }
        }


        /// <summary>
        /// Encrypts the URL DES.
        /// </summary>
        /// <param name="InputString">The input string.</param>
        /// <returns></returns>
        public static string EncryptURL_DES(string InputString)
        {
            try
            {
                //Dim mCryptProv As SymmetricAlgorithm
                MemoryStream mMemStr = default(MemoryStream);

                //Encrypt the Data in  the textbox txtData, show that in MessageBox and write back to textbox   
                //Here You can provide the name of any class which supportsSymmetricAlgorithm Like DES(mCryptProv = SymmetricAlgorithm.Create("Rijndael"))
                TripleDESCryptoServiceProvider mCryptProv = new TripleDESCryptoServiceProvider();

                //The encrypted data will be stored in the memory, so we need a memory stream object
                mMemStr = new MemoryStream();
                byte[] iv = null;
                byte[] key = null;
                key = System.Text.Encoding.ASCII.GetBytes("E$@Efd45$#Wddsf#0WSdPSD4");
                iv = System.Text.Encoding.ASCII.GetBytes("5%&ERfd4");
                //Create the ICryptTransform Object. 
                ICryptoTransform mTransform = mCryptProv.CreateEncryptor(key, iv);

                //Create the Crypto Stream for writing, and pass that MemoryStream (where to write after encryption), and ICryptoTransform Object.
                CryptoStream mCSWriter = new CryptoStream(mMemStr, mTransform, CryptoStreamMode.Write);
                //This StreamWriter object will be used to write 
                //Encrypted data to the Memory Stream 

                //Pass it the object of CryptoStream 
                StreamWriter mSWriter = new StreamWriter(mCSWriter);
                mSWriter.Write(InputString);

                //Write data after encryption 
                mSWriter.Flush();
                //Make sure to write everything from Stream 
                mCSWriter.FlushFinalBlock();
                //Flush the CryptoStream as well 

                //The Data has been written in the Memory, But we need to display that back to text box and want to show in Messagebox. So do the following

                //Create byte array to receive data
                byte[] mBytes = new byte[mMemStr.Length];
                mMemStr.Position = 0;
                //Move to beginning of data
                //Read All data from the memory
                mMemStr.Read(mBytes, 0, (int)mMemStr.Length);

                //But this data is in Bytes, and we need to convert it to string.
                //String conversion address to the problem of Encoding format. We are using UTF8 Encoding to encode data from byte to string

                string mEncData = ConvertByteArrayToString(mBytes);
                byte[] inputbytes = ConvertStringToByteArray(mEncData);

                mMemStr.Position = 0;
                mTransform = mCryptProv.CreateDecryptor(key, iv);
                CryptoStream mCSReader = new CryptoStream(mMemStr, mTransform, CryptoStreamMode.Read);
                StreamReader mStrREader = new StreamReader(mCSReader);
                string mDecData = mStrREader.ReadToEnd();

                return (HttpUtility.UrlEncode(mEncData).ToString());

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        /// <summary>
        /// Converts the byte array to string.
        /// </summary>
        /// <param name="bt">The bt.</param>
        /// <returns></returns>
        public static string ConvertByteArrayToString(byte[] bt)
        {
            return Convert.ToBase64String(bt).Replace("+", "-").Replace("/", "_");
        }

        /// <summary>
        /// Converts the string to byte array.
        /// </summary>
        /// <param name="stringToConvert">The string to convert.</param>
        /// <returns></returns>
        public static byte[] ConvertStringToByteArray(string stringToConvert)
        {
            return Convert.FromBase64String(stringToConvert.Replace("-", "+").Replace("_", "/"));
        }

        /// <summary>
        /// Encrypts to m d5 hash.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string EncryptToMD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i <= result.Length - 1; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString().ToUpper();
        }
    }
}