﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace DecintellMembershipAPI.Services
{
    public class LogDetails
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void GenerateLog(int userId, string apiName, string fileName, string status = "Error")
        {
            Log.Info("---------------------------- " + status + " --------------------------------");
            Log.Error("Date.......: " + DateTime.Now);
            Log.Error("User ID....: " + userId);
            Log.Error("File Name..: " + fileName);
            Log.Error("API Name...: " + apiName);
            //Log.Error("Stack Trace: " + stackTrace);
            //            Log.Info("------------------------------------------------------------");
        }
    }
}