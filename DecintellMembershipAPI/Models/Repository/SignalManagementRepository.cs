﻿using DecintellMembershipAPI.Models.IRepository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Reflection;
using SignalATMDataAccessAPI;

namespace DecintellMembershipAPI.Models.Repository
{
    public class SignalManagementRepository:ISignalManagementRepository

    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Repository _repository = new Repository();

        public JObject SaveNewSignal(int InstrumentId, int StlD, int SigProID, int BuySellID, decimal StrikePrice, decimal TargetPrice, 
                                decimal StopLoss, decimal InstrumentPrice, string DirectionCheck, string strComments, int MINTimeFrameID, int MaxTimeFrameID, 
                                DateTime MaxTimeFrame, int MinValidityID, int MaxValidityID, DateTime MaxValidityDate, int ReliabilityID, int RiskID,
                                int ProcessStatusID, int SuccessStatusID, decimal ClosingPips,decimal ClosingPercent, DateTime EntryDate, DateTime ClosingDate, string UpdatedBy,decimal LivePrice)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_save_InsertNewSignals(InstrumentId, StlD, SigProID, BuySellID, StrikePrice, TargetPrice,
                            StopLoss, InstrumentPrice, DirectionCheck, strComments, MINTimeFrameID, MaxTimeFrameID,
                            MaxTimeFrame, MinValidityID, MaxValidityID, MaxValidityDate, ReliabilityID, RiskID,
                            ProcessStatusID, SuccessStatusID, ClosingPips, ClosingPercent, EntryDate, ClosingDate, UpdatedBy, LivePrice).FirstOrDefault();

                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in SignalManagementRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        public JObject GetInstrumentDetailsByInstrumentID(int InstrumentID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetInstrumentDetailsByInstrumentID(InstrumentID).FirstOrDefault();

                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }


    }
}