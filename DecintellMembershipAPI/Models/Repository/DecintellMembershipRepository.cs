﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DecintellMembershipAPI.Models;
using Newtonsoft.Json.Linq;

using DecintellMembershipAPI.Models.IRepository;
using log4net;
using System.Reflection;
using System.IO;
using System.Configuration;
using SignalATMDataAccessAPI;

namespace DecintellMembershipAPI.Models.Repository
{
    public class DecintellMembershipRepository : IDecintellMembershipRepository
    {
        private readonly Repository _repository = new Repository();
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
      

        public JObject GetValidUser(string UserID, string Password)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_sec_GetAllUserByLoginIDNew(UserID, Password).FirstOrDefault();

                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception from DecintellMembershipAPI.Models.Repository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

       
    }

}