﻿using DecintellMembershipAPI.Models.IRepository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Reflection;
using SignalATMDataAccessAPI;

namespace DecintellMembershipAPI.Models.Repository
{
    /// <summary></summary>
    /// <seealso cref="DecintellMembershipAPI.Models.IRepository.IMobileAppProvidersRepository" />
    public class MobileAppProvidersRepository:IMobileAppProvidersRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Repository _repository = new Repository();

        /// <summary>Gets the time zones.</summary>
        /// <returns></returns>
        public JObject GetTimeZones()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetTimeZones().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the time zones by country identifier.</summary>
        /// <param name="CountryId">The country identifier.</param>
        /// <returns></returns>
        public JObject GetTimeZonesByCountryId(int CountryId)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetTimeZonesByCountryId(CountryId);
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the user plan.</summary>
        /// <param name="RoleId">The role identifier.</param>
        /// <returns></returns>
        public JObject GetUserPlan(int RoleId)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetUserPlan(RoleId);
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the subscription plan.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        public JObject GetSubscriptionPlan(int UserID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_Get_SubcriptionPlan(UserID);
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the personal details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        public JObject GetPersonalDetails(int UserID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetPersonalDetails(UserID);
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Updates the personal details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Email">The email.</param>
        /// <param name="UserPhoneNO">The user phone no.</param>
        /// <param name="Gender">The gender.</param>
        /// <param name="UserUrl">The user URL.</param>
        /// <param name="UserAddress">The user address.</param>
        /// <param name="CountryID">The country identifier.</param>
        /// <param name="State">The state.</param>
        /// <param name="City">The city.</param>
        /// <param name="TimeZone">The time zone.</param>
        /// <param name="AboutMe">The about me.</param>
        /// <param name="UserReferenceID">The user reference identifier.</param>
        /// <param name="UserImage">The user image.</param>
        /// <returns></returns>
        public JObject UpdatePersonalDetails(int UserID, string UserName, string Email, string UserPhoneNO, int Gender, string UserUrl, string UserAddress, int CountryID, string State, string City, float TimeZone, string AboutMe, int UserReferenceID, byte[] UserImage)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_UpdatePersonalDetails(UserID, UserName, Email, UserPhoneNO, Gender, UserUrl, UserAddress, CountryID, State, City, TimeZone, AboutMe, UserReferenceID, UserImage);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                return new JObject
                {
                    {"statusCode", 404},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Saves the personal details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Email">The email.</param>
        /// <param name="UserPhoneNO">The user phone no.</param>
        /// <param name="Gender">The gender.</param>
        /// <param name="UserUrl">The user URL.</param>
        /// <param name="UserAddress">The user address.</param>
        /// <param name="CountryID">The country identifier.</param>
        /// <param name="State">The state.</param>
        /// <param name="City">The city.</param>
        /// <param name="TimeZone">The time zone.</param>
        /// <param name="AboutMe">The about me.</param>
        /// <param name="UserReferenceID">The user reference identifier.</param>
        /// <param name="UserImage">The user image.</param>
        /// <returns></returns>
        public JObject SavePersonalDetails(int UserID, string UserName, string Email, string UserPhoneNO, int Gender, string UserUrl, string UserAddress, int CountryID, string State, string City, float TimeZone, string AboutMe, int UserReferenceID, byte[] UserImage)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_SavePersonalDetails(UserID, UserName, Email, UserPhoneNO, Gender, UserUrl, UserAddress, CountryID, State, City, TimeZone, AboutMe, UserReferenceID, UserImage);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                return new JObject
                {
                    {"statusCode", 404},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the trader compare performance.</summary>
        /// <param name="Period1">The period1.</param>
        /// <param name="Period2">The period2.</param>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        public JObject GetTraderComparePerformance(int Period1, int Period2, int UserID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_Trader_ComparePerformance(Period1, Period2, UserID);
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the signal provider perfromance details.</summary>
        /// <param name="SigProID">The sig pro identifier.</param>
        /// <param name="MarketID">The market identifier.</param>
        /// <returns></returns>
        public JObject GetSignalProviderPerfromanceDetails(int SigProID, int MarketID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_Get_SignalProviderPerformanceDeatils(SigProID, MarketID);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Uploads the user documents.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="UserRole">The user role.</param>
        /// <param name="UserPhoto">The user photo.</param>
        /// <param name="UserProfile">The user profile.</param>
        /// <param name="UserIdentity">The user identity.</param>
        /// <param name="UserAddressProof">The user address proof.</param>
        /// <param name="UserBankDetails">The user bank details.</param>
        /// <param name="UserChequeStatement">The user cheque statement.</param>
        /// <param name="IsApproved">if set to <c>true</c> [is approved].</param>
        /// <param name="FlagUserPhoto">The flag user photo.</param>
        /// <param name="FlagUserProfile">The flag user profile.</param>
        /// <param name="FlagUserIdentity">The flag user identity.</param>
        /// <param name="FlagUserAddressProof">The flag user address proof.</param>
        /// <param name="FlagUserChequeStatement">The flag user cheque statement.</param>
        /// <returns></returns>
        public JObject UploadUserDocuments(int UserID, int UserRole, byte[] imgUserPhoto, byte[] UserProfile, byte[] UserIdentity, byte[] UserAddressProof, string UserBankDetails, byte[] imgUserChequeStatement, bool IsApproved, int FlagUserPhoto, int FlagUserProfile, int FlagUserIdentity, int FlagUserAddressProof, int FlagUserChequeStatement)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_UploadUserDocuments(UserID, UserRole, imgUserPhoto, UserProfile, UserIdentity, UserAddressProof, UserBankDetails, imgUserChequeStatement, IsApproved, FlagUserPhoto, FlagUserProfile, FlagUserIdentity, FlagUserAddressProof, FlagUserChequeStatement);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                return new JObject
                {
                    {"statusCode", 404},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the user document details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        public JObject GetUserDocumentDetails(int UserID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetUserDocumentDetails(UserID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the signal library for trader.</summary>
        /// <param name="TraderID">The trader identifier.</param>
        /// <returns></returns>
        public JObject GetSignalLibraryForTrader(int TraderID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetSignalLibraryForTrader(TraderID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the pinned signals.</summary>
        /// <param name="TraderID">The trader identifier.</param>
        /// <returns></returns>
        public JObject GetPinnedSignals(int TraderID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetPinnedSignals(TraderID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the signal closed.</summary>
        /// <param name="SigProID">The sig pro identifier.</param>
        /// <returns></returns>
        public JObject GetSignalClosed(int SigProID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_Get_SignalClosed(SigProID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the ticket responses by identifier.</summary>
        /// <param name="TicketID">The ticket identifier.</param>
        /// <returns></returns>
        public JObject GetTicketResponsesById(int TicketID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetTicketResponsesById(TicketID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the responses MSG by identifier.</summary>
        /// <param name="TicketResponsesID">The ticket responses identifier.</param>
        /// <returns></returns>
        public JObject GetResponsesMsgById(int TicketResponsesID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetResponsesMsgById(TicketResponsesID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the subscribed plan details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        public JObject GetSubscribedPlanDetails(int UserID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetSubscribedPlanDetails(UserID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the promo coupons.</summary>
        /// <returns></returns>
        public JObject GetPromoCoupons()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetPromoCoupons().ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Getmsts the feedback.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <returns></returns>
        public JObject GetmstFeedback(int UserID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetmstFeedback(UserID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Saves the feedback.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="FeedbackSubject">The feedback subject.</param>
        /// <param name="FeedbackComment">The feedback comment.</param>
        /// <param name="FeedbackFile">The feedback file.</param>
        /// <returns></returns>
        public JObject SaveFeedback(int UserID, string FeedbackSubject, string FeedbackComment, byte[] FeedbackFile)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_SaveFeedback(UserID, FeedbackSubject, FeedbackComment, FeedbackFile);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }


        /// <summary>Saves the ticket details.</summary>
        /// <param name="FromID">From identifier.</param>
        /// <param name="ToID">To identifier.</param>
        /// <param name="Subject">The subject.</param>
        /// <param name="Message">The message.</param>
        /// <param name="TicketStatusID">The ticket status identifier.</param>
        /// <param name="imgChooseScreen">The img choose screen.</param>
        /// <returns></returns>
        public JObject SaveTicketDetails(int FromID, int ToID, string Subject, string Message, int TicketStatusID, byte[] imgChooseScreen)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_SaveTicketDetails(FromID, ToID, Subject, Message, TicketStatusID, imgChooseScreen);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }


        /// <summary>Saves the ticket responses.</summary>
        /// <param name="TicketID">The ticket identifier.</param>
        /// <param name="Message">The message.</param>
        /// <param name="TicketStatusID">The ticket status identifier.</param>
        /// <param name="ResponsebyID">The responseby identifier.</param>
        /// <returns></returns>
        public JObject SaveTicketResponses(int TicketID, string Message, int TicketStatusID, int ResponsebyID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_SaveTicketResponses(TicketID, Message, TicketStatusID, ResponsebyID);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the ticket status list.</summary>
        /// <returns></returns>
        public JObject GetTicketStatusList()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetTicketStatusList().ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }


        /// <summary>Gets the signal library.</summary>
        /// <param name="sigProID">The sig pro identifier.</param>
        /// <returns></returns>
        public JObject GetSignalLibrary(int sigProID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_Get_SP_SignalLibrary(sigProID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }


        /// <summary>Gets the signaldashboard.</summary>
        /// <param name="sigProID">The sig pro identifier.</param>
        /// <returns></returns>
        public JObject GetSignaldashboard(int sigProID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_Get_SignalDashbord(sigProID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }


        /// <summary>Gets the user earning cashout request.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="RoleID">The role identifier.</param>
        /// <returns></returns>
        public JObject GetUserEarningCashoutRequest(int UserID, int RoleID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetUserEarningCashoutRequest(UserID, RoleID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the user wallet balance.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="RoleID">The role identifier.</param>
        /// <returns></returns>
        public JObject GetUserWalletBalance(int UserID, int RoleID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetUserWalletBalance(UserID, RoleID).ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Saves the user cashout earning request details.</summary>
        /// <param name="UserID">The user identifier.</param>
        /// <param name="RoleID">The role identifier.</param>
        /// <param name="RequestAmout">The request amout.</param>
        /// <param name="CurrentWallentAmount">The current wallent amount.</param>
        /// <returns></returns>
        public JObject SaveUserCashoutEarningRequestDetails(int UserID, int RoleID, decimal RequestAmout, decimal CurrentWallentAmount)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_SaveUserCashoutEarningRequestDetails(UserID, RoleID, RequestAmout, CurrentWallentAmount);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }


        /// <summary>Referrers the performance deatils.</summary>
        /// <param name="RefferedID">The reffered identifier.</param>
        /// <returns></returns>
        public JObject ReferrerPerformanceDeatils(int RefferedID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_Get_ReferrerPerformanceDeatils(RefferedID);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        /// <summary>Gets the empanelment details.</summary>
        /// <param name="SignalProID">The signal pro identifier.</param>
        /// <returns></returns>
        public JObject GetEmpanelmentDetails(int SignalProID)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetEmpanelmentDetails(SignalProID);
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in MobileAppProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }
    }
}