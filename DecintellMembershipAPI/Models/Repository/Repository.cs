﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DecintellMembershipAPI.Models.Repository
{
    public class Repository
    {
        //private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public JObject Response<T>(T data, Exception ex = null, int? totalRecords = null)
        {
            try
            {
                var responseData = JsonConvert.SerializeObject(new
                {
                    data
                });
                var returnData = JObject.Parse(responseData);
                if (typeof(T) == typeof(int))
                {
                    switch (Convert.ToInt32(data))
                    {
                        case 1:
                            returnData.Add("statusCode", "200");
                            returnData.Add("error", null);
                            returnData.Add("status", true);
                            returnData.Add("message", "Success");
                            break;
                        case -10:
                            returnData.Add("statusCode", "304");
                            returnData.Add("error", "Version number mismatch");
                            returnData.Add("status", false);
                            returnData.Add("message", "Failed");
                            break;
                        case 0:
                            returnData.Add("statusCode", "503");
                            returnData.Add("error", "Something went wrong");
                            returnData.Remove("data");
                            returnData.Add("data", null);
                            returnData.Add("status", false);
                            returnData.Add("message", "Failed");
                            break;
                        case 503:
                            returnData.Add("statusCode", "503");
                            returnData.Add("error", ex.ToString());
                            returnData.Add("status", false);
                            break;
                        case 404:
                            returnData.Add("statusCode", "404");
                            returnData.Add("error", null);
                            returnData.Remove("data");
                            returnData.Add("data", "Item not found");
                            returnData.Add("status", true);
                            returnData.Add("message", "Failed");
                            break;
                        case -1:
                            returnData.Add("statusCode", "200");
                            returnData.Add("error", null);
                            returnData.Remove("data");
                            returnData.Add("data", null);
                            returnData.Add("message", "No matching SKU active service");
                            returnData.Add("status", true);
                            break;
                        case -2:
                            returnData.Add("statusCode", "200");
                            returnData.Add("error", null);
                            returnData.Remove("data");
                            returnData.Add("data", null);
                            returnData.Add("message", "LWUniqueId is already used");
                            returnData.Add("status", true);
                            break;
                        case -4:
                            returnData.Add("statusCode", "404");
                            returnData.Add("error", null);
                            returnData.Remove("data");
                            returnData.Add("data", null);
                            returnData.Add("message", "WO lost by this supplier");
                            returnData.Add("status", true);
                            break;
                        case -5:
                            returnData.Add("statusCode", "200");
                            returnData.Add("error", null);
                            returnData.Remove("data");
                            returnData.Add("data", null);
                            returnData.Add("message", "WO won by this supplier");
                            returnData.Add("status", true);
                            break;
                        case -6:
                            returnData.Add("statusCode", "304");
                            returnData.Add("error", null);
                            returnData.Remove("data");
                            returnData.Add("data", null);
                            returnData.Add("message", "WO already updated");
                            returnData.Add("status", true);
                            break;
                    }
                }
                else
                {
                    returnData.Add("statusCode", "200");
                    if (totalRecords != null)
                    {
                        returnData.Add("totalRecords", totalRecords);
                    }
                    returnData.Add("error", null);
                    returnData.Add("status", true);
                    returnData.Add("message", "Success");
                }


                var serializer = new JsonSerializer()
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
                var jo = JObject.FromObject(returnData, serializer);
                return jo;
            }
            catch (Exception exception)
            {
               // Log.Error(exception);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", exception.Message},
                    {"data", null},
                    {"status", false},
                    {"message", "Failed"},

                };
            }
        }
    }
}