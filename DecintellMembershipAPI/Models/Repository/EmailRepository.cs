﻿using DecintellMembershipAPI.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DecintellMembershipAPI.Models.Repository
{
    public class EmailRepository
    {
        SMTPEmailHelper smtpEmailHelper;
        SMTPEmailBodyBuilder smtpEmailBodyBuilder;
        List<string> _listCCs = null;
        List<string> _listBCCs = null;
        System.Net.Mail.Attachment attachment = null;

        public void SendPasswordResetEmail(string mailTo, string mailSubject, string mailBody)
        {
            smtpEmailHelper = new SMTPEmailHelper();
            smtpEmailBodyBuilder = new SMTPEmailBodyBuilder();
            smtpEmailHelper.ConfigSendEmail(mailTo, smtpEmailBodyBuilder.GetPassqwordResetEmailBody(mailTo), mailBody, _listCCs,
             _listBCCs, attachment);
        }
        public void SendRegistartionEmail(string mailTo, string mailSubject)
        {
            smtpEmailHelper = new SMTPEmailHelper();
            smtpEmailBodyBuilder = new SMTPEmailBodyBuilder();
            smtpEmailHelper.ConfigSendEmail(mailTo, mailSubject, smtpEmailBodyBuilder.GetRegistrationCompleteEmailBody(), _listCCs,
             _listBCCs, attachment);
        }
        public void SendActivationEmail(string mailTo, string mailSubject, string url)
        {
            smtpEmailHelper = new SMTPEmailHelper();
            smtpEmailBodyBuilder = new SMTPEmailBodyBuilder();
          //  string url = "https://localhost:44352/Home/ActivateUserAccount?email="+mailTo;
            smtpEmailHelper.ConfigSendEmail(mailTo, mailSubject, smtpEmailBodyBuilder.GetActivationEmailBody(mailTo,url), _listCCs,
            _listBCCs, attachment);
        }

        public void SendPasswordResetEmaillink(string mailTo, string mailSubject, string mailBody, string url)
        {
            smtpEmailHelper = new SMTPEmailHelper();
            smtpEmailBodyBuilder = new SMTPEmailBodyBuilder();
            // string url = "https://localhost:44352/Home/ActivateUserAccount?email=" + mailTo;
            smtpEmailHelper.ConfigSendEmail(mailTo, mailSubject, smtpEmailBodyBuilder.GetActivationPasswordBody(mailTo, url), _listCCs,
            _listBCCs, attachment);
        }
    }
}