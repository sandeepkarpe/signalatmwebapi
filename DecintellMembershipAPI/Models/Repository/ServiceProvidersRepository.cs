﻿using DecintellMembershipAPI.Models.IRepository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Reflection;
using SignalATMDataAccessAPI;

namespace DecintellMembershipAPI.Models.Repository
{   
    public class ServiceProvidersRepository: IServiceProvidersRepository
    {

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Repository _repository = new Repository();

        public JObject GetCountries()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetCountry().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        public JObject GetMarkets()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetMarkets().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }
        public JObject GetRisks()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetAllRisks().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }
        public JObject GetAllInstrumentsbyMarketId(int MarketId)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetAllInstrumentsbyMarketId(MarketId).ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }
        public JObject GetAllSignalType()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetSignalTypes().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }
        public JObject GetAllRisks()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetAllRisks().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        public JObject GetAllPeriods()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetPeriodsList().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        public JObject GetAllTimeFrames()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetTimeFrames().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        public JObject GetAllReliability()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetAllReliability().ToList();
                    return _repository.Response(clientResult);
                }
            }

            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

        public JObject GetBuySellList()
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var clientResult = dbConn.usp_GetBuySellList().ToList();
                    return _repository.Response(clientResult);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception in ServiceProvidersRepository", e);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", e.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }
        public JObject SaveBasicConfig(int countryId, int timeZoneId, int userId, int marketId, string intsArr)
        {
            try
            {
                using (var dbConn = new SignalATMEntities())
                {
                    var responseString = dbConn.usp_Save_BasicConfiguration(countryId, timeZoneId, userId, marketId, intsArr);
                    return _repository.Response(responseString);
                }

            }
            catch (Exception ex)
            {
                Log.Error("Exception in ServiceProvidersRepository", ex);
                return new JObject
                {
                    {"statusCode", 503},
                    {"error", ex.Message},
                    {"data", null},
                    {"status", false}
                };
            }
        }

    }
}